<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function testConnect()
    {
        try {
            DB::connection()->getPdo();
            return apiSuccess('Success');
        } catch (\Exception $e) {
            return apiError(null,"Could not connect to the database.  Please check your configuration. error:" . $e);
        }
    }
}
