<?php

if (!function_exists('formatDate')) {
    function formatDate($date, string $format = 'Y/m/d')
    {
        if ($date instanceof \Carbon\Carbon) {
            return $date->format($format);
        }
        return $date;
    }
}

if (!function_exists('apiSuccess')) {
    function apiSuccess($data, $message = null){
        return response()->json([
            'status' =>true,
            // 'message' => $message,
            'data' => $data,
        ], 200); 
    }
}

if (!function_exists('apiError')) {
    function apiError($errorCode, $message = null, $code = 400){
        return response()->json([
            'status' =>false,
            'message' => $message,
            'code' => $code,
        ], $errorCode); 
    }
}